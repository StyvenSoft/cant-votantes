#include <stdio.h>
#include <stdlib.h>

//Se instancian las funciones votar(), validaci�n() y datos()

//La funci�n votar es la encargada de validar y registrar el numero del candidato por el cual se va a votar (1, 2, 3)
int votar();
//Esta funcion validacion es la encargada de verificar el numero de identificaci�n del votante
int validacion();
//Esta funci�n datos es la que instancia todos los datos requeridos para poder consolidar la informaci�n
void datos(char nombre [][35], int identificacion [], char rol [][15], int  voto[], int cantidad, int i, int votoCandidato1, int votoCandidato2, int votoCandidato3);

//Esta funci�n main() es la que sirve de punto de ejecuccion del programa inicial.
int main(){

    // se define la variable de la cantidad de votantes
    int cantidad;
    int i;
    //Se describe un titulo del prorama
    printf("ELECCIONES RECTOR UNIVERSIDAD TUX \n");
    //Se solicita la cantidad de votantes
    printf("Digite la cantidad de votantes \n");
    scanf("%d", &cantidad);
    //Se definen las variables que se usaran para capturar los datos
    char nombre [cantidad][35] ;
    int  identificacion [cantidad];
    char rol [cantidad][15];
    int voto[cantidad];
    int votoCandidato1=0;
    int votoCandidato2=0;
    int votoCandidato3=0;



    //se ejecuta un ciclo de repetici�n donde se registran los datos del votante
     for (i=0; i<cantidad; i++){
            printf("Registro votante numero: %d\n",i+1);
            printf("Digite el nombre del votante: \n");
            scanf("%s",&nombre[i]);
            fflush(stdin);
            identificacion[i]=validacion();
            printf("\nDigite el rol dentro de la Univerdidad: (Estudiante, Docente, Administrativo)\n");
            scanf("%s]",&rol[i]);
            //llamada a la funci�n que valida el voto
            voto[i]=votar();
            // se ejecuta una sentencia condicional para guardar la cantidad de los votos seg�n sea el cantidato elegido (1,2,3)
                if(voto[i] == 1){
                    votoCandidato1 = votoCandidato1 + 1;
                }
                else if(voto[i] == 2){
                    votoCandidato2 = votoCandidato2 + 1;
                }
                else{
                    votoCandidato3 = votoCandidato3 + 1;
                }
            }


    //se hace la llamada al metodo que gurada todos los datos registrados anteriormente
     datos(nombre , identificacion, rol, voto, cantidad, i, votoCandidato1, votoCandidato2, votoCandidato3);
     return 0;

}

//se define la funci�n votar()
int votar(){
    int digito;
    do
    {
        printf("\nDigite el numero del candidato rector por el que votara: (1, 2, 3)\n");
        scanf("%d",&digito);
    // cliclo que valida que el numero de voto sea (1,2,3) o de lo contrario se ejecuta de nuevo el registro
    }while ((digito<=0) || (digito>=4));
    printf("Se ha registrado correctamente!\n");
    //se retorna el numero de votaci�n
    return digito;
}

// se define la funcion validaci�n
int validacion(){
    int digito;
    do
    {
        printf("Numero de identificacion: \n");
        scanf("%d",&digito);
    }while (digito<0);

    return digito;
}

//se define la funci�n datos() donde se intancia los parametros de la informaci�n registrada para dar el consolidado de la votaci�n
void datos(char nombre [][35], int  identificacion [], char rol [][15], int  voto[], int cantidad, int i, int votoCandidato1, int votoCandidato2, int votoCandidato3 ){
            //se imprimen los datos de cada votante
            printf("\n Condolidado de votacion\n");
            //cliclo que retorna la informaci�n de los votantes
            for (i=0; i<cantidad; i++){
            printf("Nombre del votante: %s\n",nombre[i]);
            printf("Numero de identificacion: %d\n",identificacion[i]);
            printf("Rol que efectua: %s\n",rol[i]);
            printf("voto: %d\n",voto[i]);

     }
     //Se imprime la cantidad de votos registrados para cada uno de los candidatos.
    printf("Cantidad de votos por candidato\n:");
    printf("Votos por candidato numero 1: %i\n", votoCandidato1);
    printf("Votos por candidato numero 2: %i\n", votoCandidato2);
    printf("Votos por candidato numero 3: %i\n", votoCandidato3);
}
